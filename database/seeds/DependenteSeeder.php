<?php

use Illuminate\Database\Seeder;

class DependenteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table ('dependentes')->insert(['nome' => 'Haleigh', 'data_nascimento' => '2014-05-15', 'pessoa_id' => '1' ]);
        DB::table ('dependentes')->insert(['nome' => 'Julissa', 'data_nascimento' => '2017-05-26', 'pessoa_id' => '1' ]);
        DB::table ('dependentes')->insert(['nome' => 'Normie', 'data_nascimento' => '2007-05-14', 'pessoa_id' => '2' ]);
        DB::table ('dependentes')->insert(['nome' => 'Camellia', 'data_nascimento' => '2011-09-08', 'pessoa_id' => '2' ]);
        DB::table ('dependentes')->insert(['nome' => 'Annelise', 'data_nascimento' => '2002-12-19', 'pessoa_id' => '3' ]);
        DB::table ('dependentes')->insert(['nome' => 'Madelina', 'data_nascimento' => '2013-03-04', 'pessoa_id' => '3' ]);
        DB::table ('dependentes')->insert(['nome' => 'Dell', 'data_nascimento' => '2012-12-22', 'pessoa_id' => '4' ]);
        DB::table ('dependentes')->insert(['nome' => 'Franky', 'data_nascimento' => '2018-07-05', 'pessoa_id' => '4' ]);
        DB::table ('dependentes')->insert(['nome' => 'Timmy', 'data_nascimento' => '2007-02-14', 'pessoa_id' => '5' ]);
        DB::table ('dependentes')->insert(['nome' => 'Brnaby', 'data_nascimento' => '2001-12-31', 'pessoa_id' => '5' ]);
        DB::table ('dependentes')->insert(['nome' => 'Carmina', 'data_nascimento' => '2001-12-25', 'pessoa_id' => '6' ]);
        DB::table ('dependentes')->insert(['nome' => 'Filip', 'data_nascimento' => '2004-04-15', 'pessoa_id' => '6' ]);
        DB::table ('dependentes')->insert(['nome' => 'Gaelan', 'data_nascimento' => '2016-05-18', 'pessoa_id' => '7' ]);
        DB::table ('dependentes')->insert(['nome' => 'Norman', 'data_nascimento' => '2006-02-18', 'pessoa_id' => '7' ]);
        DB::table ('dependentes')->insert(['nome' => 'Tuesday', 'data_nascimento' => '2018-04-16', 'pessoa_id' => '8' ]);
        DB::table ('dependentes')->insert(['nome' => 'Jeana', 'data_nascimento' => '2002-09-23', 'pessoa_id' => '8' ]);
        DB::table ('dependentes')->insert(['nome' => 'Gwenni', 'data_nascimento' => '2016-08-01', 'pessoa_id' => '9' ]);
        DB::table ('dependentes')->insert(['nome' => 'Fergus', 'data_nascimento' => '2000-09-24', 'pessoa_id' => '9' ]);
        DB::table ('dependentes')->insert(['nome' => 'Brandie', 'data_nascimento' => '2018-05-21', 'pessoa_id' => '10' ]);
        DB::table ('dependentes')->insert(['nome' => 'Jaimie', 'data_nascimento' => '2006-06-01', 'pessoa_id' => '10' ]);
        DB::table ('dependentes')->insert(['nome' => 'Marshall', 'data_nascimento' => '2016-05-07', 'pessoa_id' => '11' ]);
        DB::table ('dependentes')->insert(['nome' => 'Nehemiah', 'data_nascimento' => '2015-01-10', 'pessoa_id' => '11' ]);
        DB::table ('dependentes')->insert(['nome' => 'Justin', 'data_nascimento' => '2014-07-08', 'pessoa_id' => '12' ]);
        DB::table ('dependentes')->insert(['nome' => 'Cesare', 'data_nascimento' => '2011-05-17', 'pessoa_id' => '12' ]);
        DB::table ('dependentes')->insert(['nome' => 'Bogart', 'data_nascimento' => '2004-02-11', 'pessoa_id' => '13' ]);
        DB::table ('dependentes')->insert(['nome' => 'Carmelle', 'data_nascimento' => '2013-03-13', 'pessoa_id' => '13' ]);
        DB::table ('dependentes')->insert(['nome' => 'Ali', 'data_nascimento' => '2001-04-25', 'pessoa_id' => '14' ]);
        DB::table ('dependentes')->insert(['nome' => 'Alyda', 'data_nascimento' => '2004-09-24', 'pessoa_id' => '14' ]);
        DB::table ('dependentes')->insert(['nome' => 'Putnam', 'data_nascimento' => '2001-01-08', 'pessoa_id' => '15' ]);
        DB::table ('dependentes')->insert(['nome' => 'Theda', 'data_nascimento' => '2004-09-17', 'pessoa_id' => '15' ]);
        DB::table ('dependentes')->insert(['nome' => 'Felipa', 'data_nascimento' => '2009-10-30', 'pessoa_id' => '16' ]);
        DB::table ('dependentes')->insert(['nome' => 'Nial', 'data_nascimento' => '2018-04-20', 'pessoa_id' => '16' ]);
        DB::table ('dependentes')->insert(['nome' => 'Randa', 'data_nascimento' => '2004-07-06', 'pessoa_id' => '17' ]);
        DB::table ('dependentes')->insert(['nome' => 'Rodie', 'data_nascimento' => '2001-03-13', 'pessoa_id' => '18' ]);
        DB::table ('dependentes')->insert(['nome' => 'Ross', 'data_nascimento' => '2009-04-17', 'pessoa_id' => '19' ]);
        DB::table ('dependentes')->insert(['nome' => 'Nicolai', 'data_nascimento' => '2016-07-27', 'pessoa_id' => '20' ]);
        DB::table ('dependentes')->insert(['nome' => 'Ilka', 'data_nascimento' => '2004-02-24', 'pessoa_id' => '20' ]);
        DB::table ('dependentes')->insert(['nome' => 'Stacia', 'data_nascimento' => '2017-06-01', 'pessoa_id' => '21' ]);
        DB::table ('dependentes')->insert(['nome' => 'Abie', 'data_nascimento' => '2017-05-01', 'pessoa_id' => '21' ]);
        DB::table ('dependentes')->insert(['nome' => 'Tab', 'data_nascimento' => '2010-07-17', 'pessoa_id' => '22' ]);
        DB::table ('dependentes')->insert(['nome' => 'Fredra', 'data_nascimento' => '2009-07-15', 'pessoa_id' => '22' ]);
        DB::table ('dependentes')->insert(['nome' => 'Karel', 'data_nascimento' => '2018-09-27', 'pessoa_id' => '23' ]);
        DB::table ('dependentes')->insert(['nome' => 'Auguste', 'data_nascimento' => '2002-04-18', 'pessoa_id' => '23' ]);
        DB::table ('dependentes')->insert(['nome' => 'Lorin', 'data_nascimento' => '2017-12-20', 'pessoa_id' => '24' ]);
        DB::table ('dependentes')->insert(['nome' => 'Jilli', 'data_nascimento' => '2006-07-20', 'pessoa_id' => '24' ]);
        DB::table ('dependentes')->insert(['nome' => 'Arman', 'data_nascimento' => '2001-03-07', 'pessoa_id' => '25' ]);
        DB::table ('dependentes')->insert(['nome' => 'Otha', 'data_nascimento' => '2007-09-04', 'pessoa_id' => '25' ]);
        DB::table ('dependentes')->insert(['nome' => 'Joelle', 'data_nascimento' => '2015-01-22', 'pessoa_id' => '26' ]);
        DB::table ('dependentes')->insert(['nome' => 'Aldon', 'data_nascimento' => '2014-12-15', 'pessoa_id' => '26' ]);
        DB::table ('dependentes')->insert(['nome' => 'Layton', 'data_nascimento' => '2013-08-08', 'pessoa_id' => '27' ]);
        DB::table ('dependentes')->insert(['nome' => 'Donni', 'data_nascimento' => '2016-09-22', 'pessoa_id' => '27' ]);
        DB::table ('dependentes')->insert(['nome' => 'Clair', 'data_nascimento' => '2015-11-28', 'pessoa_id' => '28' ]);
        DB::table ('dependentes')->insert(['nome' => 'Ramonda', 'data_nascimento' => '2010-06-07', 'pessoa_id' => '28' ]);
        DB::table ('dependentes')->insert(['nome' => 'Carmina', 'data_nascimento' => '2017-06-20', 'pessoa_id' => '29' ]);
        DB::table ('dependentes')->insert(['nome' => 'Alfreda', 'data_nascimento' => '2008-06-04', 'pessoa_id' => '29' ]);
        DB::table ('dependentes')->insert(['nome' => 'Bobinette', 'data_nascimento' => '2008-06-08', 'pessoa_id' => '30' ]);
        DB::table ('dependentes')->insert(['nome' => 'Margi', 'data_nascimento' => '2016-10-16', 'pessoa_id' => '30' ]);
        DB::table ('dependentes')->insert(['nome' => 'Nessie', 'data_nascimento' => '2007-03-23', 'pessoa_id' => '30' ]);
        DB::table ('dependentes')->insert(['nome' => 'Laird', 'data_nascimento' => '2015-10-03', 'pessoa_id' => '31' ]);
        DB::table ('dependentes')->insert(['nome' => 'Myrwyn', 'data_nascimento' => '2002-06-24', 'pessoa_id' => '31' ]);
        DB::table ('dependentes')->insert(['nome' => 'Dar', 'data_nascimento' => '2016-07-09', 'pessoa_id' => '32' ]);
        DB::table ('dependentes')->insert(['nome' => 'Delinda', 'data_nascimento' => '2019-05-16', 'pessoa_id' => '32' ]);
        DB::table ('dependentes')->insert(['nome' => 'Say', 'data_nascimento' => '2018-09-10', 'pessoa_id' => '33' ]);
        DB::table ('dependentes')->insert(['nome' => 'Theresina', 'data_nascimento' => '2004-04-04', 'pessoa_id' => '34' ]);
        DB::table ('dependentes')->insert(['nome' => 'Orelia', 'data_nascimento' => '2015-12-28', 'pessoa_id' => '35' ]);
        DB::table ('dependentes')->insert(['nome' => 'Arda', 'data_nascimento' => '2004-05-22', 'pessoa_id' => '35' ]);
        DB::table ('dependentes')->insert(['nome' => 'Sephira', 'data_nascimento' => '2001-01-08', 'pessoa_id' => '34' ]);
        DB::table ('dependentes')->insert(['nome' => 'Ericha', 'data_nascimento' => '2006-03-14', 'pessoa_id' => '36' ]);
        DB::table ('dependentes')->insert(['nome' => 'Willy', 'data_nascimento' => '2004-04-22', 'pessoa_id' => '36' ]);
        DB::table ('dependentes')->insert(['nome' => 'Maje', 'data_nascimento' => '2014-02-13', 'pessoa_id' => '37' ]);
        DB::table ('dependentes')->insert(['nome' => 'Lodovico', 'data_nascimento' => '2005-02-09', 'pessoa_id' => '37' ]);
        DB::table ('dependentes')->insert(['nome' => 'Addie', 'data_nascimento' => '2003-01-01', 'pessoa_id' => '38' ]);
        DB::table ('dependentes')->insert(['nome' => 'Kimberley', 'data_nascimento' => '2006-06-18', 'pessoa_id' => '38' ]);
        DB::table ('dependentes')->insert(['nome' => 'Pearl', 'data_nascimento' => '2001-07-10', 'pessoa_id' => '39' ]);
        DB::table ('dependentes')->insert(['nome' => 'Ingeborg', 'data_nascimento' => '2019-05-10', 'pessoa_id' => '39' ]);
        DB::table ('dependentes')->insert(['nome' => 'Eliza', 'data_nascimento' => '2006-09-21', 'pessoa_id' => '40' ]);
        DB::table ('dependentes')->insert(['nome' => 'Janis', 'data_nascimento' => '2016-03-06', 'pessoa_id' => '40' ]);
        DB::table ('dependentes')->insert(['nome' => 'Humphrey', 'data_nascimento' => '2010-02-16', 'pessoa_id' => '41' ]);
        DB::table ('dependentes')->insert(['nome' => 'Emmott', 'data_nascimento' => '2003-11-08', 'pessoa_id' => '41' ]);
        DB::table ('dependentes')->insert(['nome' => 'Ynes', 'data_nascimento' => '2013-03-19', 'pessoa_id' => '42' ]);
        DB::table ('dependentes')->insert(['nome' => 'Galvin', 'data_nascimento' => '2001-09-04', 'pessoa_id' => '42' ]);
        DB::table ('dependentes')->insert(['nome' => 'Miltie', 'data_nascimento' => '2001-04-03', 'pessoa_id' => '43' ]);
        DB::table ('dependentes')->insert(['nome' => 'Melonie', 'data_nascimento' => '2004-11-06', 'pessoa_id' => '43' ]);
        DB::table ('dependentes')->insert(['nome' => 'Stevana', 'data_nascimento' => '2017-06-22', 'pessoa_id' => '44' ]);
        DB::table ('dependentes')->insert(['nome' => 'Anders', 'data_nascimento' => '2009-08-14', 'pessoa_id' => '45' ]);
        DB::table ('dependentes')->insert(['nome' => 'Reuben', 'data_nascimento' => '2011-07-28', 'pessoa_id' => '45' ]);
        DB::table ('dependentes')->insert(['nome' => 'Enrichetta', 'data_nascimento' => '2015-03-30', 'pessoa_id' => '46' ]);
        DB::table ('dependentes')->insert(['nome' => 'Davida', 'data_nascimento' => '2010-11-22', 'pessoa_id' => '46' ]);
        DB::table ('dependentes')->insert(['nome' => 'Marys', 'data_nascimento' => '2002-02-25', 'pessoa_id' => '47' ]);
        DB::table ('dependentes')->insert(['nome' => 'Irena', 'data_nascimento' => '2001-07-11', 'pessoa_id' => '47' ]);
        DB::table ('dependentes')->insert(['nome' => 'Leola', 'data_nascimento' => '2018-09-29', 'pessoa_id' => '48' ]);
        DB::table ('dependentes')->insert(['nome' => 'Tiffy', 'data_nascimento' => '2014-07-23', 'pessoa_id' => '48' ]);
        DB::table ('dependentes')->insert(['nome' => 'Montague', 'data_nascimento' => '2007-06-10', 'pessoa_id' => '49' ]);
        DB::table ('dependentes')->insert(['nome' => 'Federica', 'data_nascimento' => '2005-07-05', 'pessoa_id' => '49' ]);
        DB::table ('dependentes')->insert(['nome' => 'Rudiger', 'data_nascimento' => '2018-12-28', 'pessoa_id' => '50' ]);
        DB::table ('dependentes')->insert(['nome' => 'Sherri', 'data_nascimento' => '2001-02-05', 'pessoa_id' => '2' ]);
        DB::table ('dependentes')->insert(['nome' => 'Teddie', 'data_nascimento' => '2002-10-02', 'pessoa_id' => '5' ]);
        DB::table ('dependentes')->insert(['nome' => 'Sheelagh', 'data_nascimento' => '2011-10-21', 'pessoa_id' => '50' ]);
        DB::table ('dependentes')->insert(['nome' => 'Mariel', 'data_nascimento' => '2010-10-17', 'pessoa_id' => '1' ]);
        DB::table ('dependentes')->insert(['nome' => 'Debby', 'data_nascimento' => '2015-06-04', 'pessoa_id' => '1' ]);


    }
}
