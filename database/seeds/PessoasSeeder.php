<?php

use Illuminate\Database\Seeder;

class PessoasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table ('pessoas')->insert(['nome'=> 'Janenna', 'data_nascimento' => '1991-11-28', 'email' => 'gzywicki0@yellowpages.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Melicent', 'data_nascimento' => '2002-07-22', 'email' => 'awallworth1@cisco.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Ofilia', 'data_nascimento' => '1998-07-10', 'email' => 'dpullman2@newsvine.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Elsworth', 'data_nascimento' => '2005-07-05', 'email' => 'rharmond3@linkedin.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Merv', 'data_nascimento' => '1980-09-16', 'email' => 'ydonaho4@tripadvisor.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Charla', 'data_nascimento' => '1996-01-20', 'email' => 'wizkovici5@businesswire.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Grove', 'data_nascimento' => '2013-04-29', 'email' => 'bkynson6@artisteer.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Mufinella', 'data_nascimento' => '1988-02-06', 'email' => 'ceverist7@cam.ac.uk', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Ardelis', 'data_nascimento' => '2017-05-09', 'email' => 'estrauss8@oakley.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Erna', 'data_nascimento' => '1989-02-15', 'email' => 'rpalin9@apple.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Rickie', 'data_nascimento' => '2007-12-13', 'email' => 'ctarberta@nbcnews.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Rosco', 'data_nascimento' => '1986-11-14', 'email' => 'bsparyb@icio.us', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Jacquelynn', 'data_nascimento' => '1984-04-01', 'email' => 'cipgravec@blinklist.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Hermine', 'data_nascimento' => '2016-04-15', 'email' => 'ydescroixd@nbcnews.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Wright', 'data_nascimento' => '1983-10-29', 'email' => 'atowerse@yellowpages.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Iona', 'data_nascimento' => '2000-09-21', 'email' => 'amackiesonf@ftc.gov', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Rudd', 'data_nascimento' => '1999-09-26', 'email' => 'tswayslandg@tuttocitta.it', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Penn', 'data_nascimento' => '1983-11-08', 'email' => 'jshreveh@nifty.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Jeanette', 'data_nascimento' => '1984-01-09', 'email' => 'ibeddalli@engadget.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Drucill', 'data_nascimento' => '1995-05-17', 'email' => 'dludmannj@chronoengine.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Christen', 'data_nascimento' => '2015-08-24', 'email' => 'msilverstonek@microsoft.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Darren', 'data_nascimento' => '2006-05-30', 'email' => 'tjurgesl@netscape.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Spenser', 'data_nascimento' => '2017-03-21', 'email' => 'heffnertm@360.cn', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Robyn', 'data_nascimento' => '1990-01-21', 'email' => 'cbaskwelln@umn.edu', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Tabb', 'data_nascimento' => '2009-09-18', 'email' => 'lgrimeso@indiegogo.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Legra', 'data_nascimento' => '1999-06-06', 'email' => 'msmallcombp@google.it', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Harwell', 'data_nascimento' => '2008-10-15', 'email' => 'dloakesq@bloglines.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Torrey', 'data_nascimento' => '1994-02-19', 'email' => 'fyouingsr@pbs.org', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Thoma', 'data_nascimento' => '1994-03-18', 'email' => 'adeamayas@dot.gov', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Christoffer', 'data_nascimento' => '2017-12-05', 'email' => 'bnuttont@barnesandnoble.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Ofelia', 'data_nascimento' => '1985-11-29', 'email' => 'fgileu@opera.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Dugald', 'data_nascimento' => '2005-12-29', 'email' => 'eludmanv@tmall.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Laurette', 'data_nascimento' => '1988-05-11', 'email' => 'pkeyworthw@about.me', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Terrill', 'data_nascimento' => '1988-10-10', 'email' => 'eoldeyx@nasa.gov', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Ilse', 'data_nascimento' => '1987-08-19', 'email' => 'ucamousy@abc.net.au', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Ayn', 'data_nascimento' => '1986-09-14', 'email' => 'ebellangerz@edublogs.org', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Ursulina', 'data_nascimento' => '2008-12-14', 'email' => 'dgarrould10@cafepress.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Emelina', 'data_nascimento' => '1985-05-20', 'email' => 'ebolf11@xrea.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Faydra', 'data_nascimento' => '1986-06-15', 'email' => 'ggarlett12@ucoz.ru', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Lizzy', 'data_nascimento' => '2018-03-04', 'email' => 'rarnaudet13@nhs.uk', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Alexandros', 'data_nascimento' => '1980-09-02', 'email' => 'mrodnight14@dell.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Jameson', 'data_nascimento' => '1993-08-08', 'email' => 'smotten15@shareasale.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Devin', 'data_nascimento' => '1985-06-03', 'email' => 'zdoumenc16@seesaa.net', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Nara', 'data_nascimento' => '2000-09-29', 'email' => 'ltwinberrow17@topsy.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Maurene', 'data_nascimento' => '1991-04-22', 'email' => 'ekleinpeltz18@dot.gov', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Helena', 'data_nascimento' => '2000-04-06', 'email' => 'fingledew19@patch.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Hobard', 'data_nascimento' => '1992-07-21', 'email' => 'iruslin1a@paypal.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Karlene', 'data_nascimento' => '1995-03-29', 'email' => 'dcolley1b@dot.gov', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Krissie', 'data_nascimento' => '1980-09-05', 'email' => 'trizzolo1c@nationalgeographic.com', 'status' => true,'foto' => '/images/default.png']);
        DB::table ('pessoas')->insert(['nome'=> 'Gabrila', 'data_nascimento' => '2010-02-03', 'email' => 'ipepin1d@oracle.com', 'status' => true,'foto' => '/images/default.png']);

    }
}
