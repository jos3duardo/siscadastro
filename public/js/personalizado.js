//inicio do script que dispara dois enventos no click do botao de excluir pessoas com o checkbox marcado
    function Aviso() {
        return confirm('Ao excluir uma pessoa todos os dependetes também serão excluido! Deseja continuar');
    }
    function Enviar() {
        document.getElementById('formulario').submit();
    }
//fim do script que dispara dois enventos no click do botao de excluir pessoas com o checkbox marcado

//inicio da função que marca e desmarca os checkbox para a deletar em lote
    $(function () {
        $('#checkAll').change(function(){
            if (this.checked) {
                $('.chM').attr({ checked: true });
            } else {
                $('.chM').attr({ checked: false });
            }
        });
        $('.chM').change(function(){
            if ($('#main').attr('checked') == true) {
                $("#main").attr({ checked: false })
            }
        });
    });
//fim da função que marca e desmarca os checkbox para a deletar em lote
