<?php

//pagina inicial
Route::get('/', 'PaginaController@index')->name('index');
//view com a lista de todas as pessoas cadastradas
Route::get('/lista', 'PaginaController@lista')->name('lista');
//direciona para o form de cadastro de pessoa
Route::get('/form', 'PaginaController@create')->name('cadastro');
//cadastra uma pessoa
Route::post('/form/add', 'PaginaController@store')->name('addCadastro');
//direciona para a view de editar pessoa com os dados da pessoa
Route::get('/pessoa/edit/{id}', 'PaginaController@edit')->name('editarCadastro');
//edita a pessoa
Route::post('/pessoa/update/{id}', 'PaginaController@update')->name('updateCadastro');
//apaga se o check box estiver marcado
Route::get('/pessoa/delete/checkbox/', 'PaginaController@apagaCheckBox')->name('apagaCheckBox');
//mostra uma view com os dados da pessoas e seus dependentes
Route::get('/dependente/{id}', 'PaginaController@dependente')->name('dependente');
//adiciona um dependente a pessoa
Route::post('/dependente/add/{id}', 'PaginaController@addDependente')->name('addDependente');
//delete um dependente
Route::get('/dependente/delete/{id}/{pessoa}', 'PaginaController@deleteDependente')->name('deleteDependente');
//muda o status e retorna para a view lista
Route::get('/status/{id}', 'PaginaController@status')->name('status');
