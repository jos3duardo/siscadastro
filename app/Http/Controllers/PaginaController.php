<?php

namespace App\Http\Controllers;

use App\Dependente;
use App\Pessoa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PaginaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Pagina.index');
    }

    //direciona para a listagem de pessoas cadastradas
    public function lista(){
        $pessoa = Pessoa::paginate(10);
        $totalPaginas = $pessoa->lastPage();
        return view('Cadastro.lista', compact('pessoa','totalPaginas'));
    }
    /**
     * Direciona para uma view de cadastro de pessoa.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Cadastro.form');
    }
    /**
     * Cria um novo cadastro de pessoa
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //valida a idade da pessoa que esta sendo cadastrada
        if (isset( $request['cDataNasc'])){
            //pegando o atributo da data_nascimento para fazer a validação da idade
            $data = explode("-", $request['cDataNasc']);
            $ano = $data[0];
            $data_atual = date('Y');
            //calcula a idade
            $idade = $data_atual - $ano;
                if ($idade >= 120){
                    return redirect('form')->with('status','Vocês está tentando cadstrar uma pessoa com mais de 120 anos, ou com uma data inválida. Por favor confira a data e tente novamente!');
                }
        }

        //regra de validação
        $regras = [
            'cFoto' => 'max:200',
        ];
        //mensagem personalizada
        $mensagen = [
            'cFoto.max' => 'A foto não pode ter o tamanho maior que 200kb',
        ];

        //valida os campos de acordo com as regras e mostra a mensagem personalizada
        $request->validate($regras,$mensagen);

        //verifica se esta sendo enviado uma foto
        if (!$request->file('cFoto') == '') // verifica se a foto foi alterada
        {
            $foto = $request->file('cFoto')->store('images','public');
        }
        else{
            //caso não venha nenhuma foto usa uma padrão do sistema
            $foto = '/images/default.png';
        }
        //cria e salva a pessoa de acorodo com os dados que são passados
        $pessoa = new Pessoa();
        $pessoa->nome = $request->input('cNome');
        $pessoa->data_nascimento = $request->input('cDataNasc');
        $pessoa->email = $request->input('cEmail');
        $pessoa->status = true;
        $pessoa->foto = $foto;
        $pessoa->save();

        return redirect(route('lista'));
    }

    //direciona para a listagem de dependetes
    public function dependente($id){
        $pessoa = Pessoa::find($id);
        $dependentes = Dependente::where([
            'pessoa_id' => $id
        ])->get();

        return view('Cadastro.dependentes', compact('pessoa','dependentes'));
    }

    //adiciona um dependete para a pessoa e retorna para a vire de cadastro de dependente
    public function addDependente(Request $request, $id)
    {
        //pegando o atributo da data_nascimento para fazer a validação da idade
        $data = explode("-", $request['cDataNasc']);
        $ano = $data[0];
        $data_atual = date('Y');
        //calcula a idade
        $idade = $data_atual - $ano;

        //valida a idade do dependente que esta sendo cadastrada
        if ($idade >= 120){
            return redirect(route('dependente',['id' => $id]))->with('status','Vocês está tentando cadstrar um dependente com mais de 120 anos, ou com uma data inválida. Por favor confira a data e tente novamente!');
        }
        //procura a pessoa ao qual o depentende sera vinculado
        $pessoa = Pessoa::find($id);
        /*checa o status para ver se o cadastro esta ativo
        caso não esteja o sistema não deixa realizar o cadastro de dependentes*/
        if ($pessoa->status == false){
            return redirect(route('dependente',['id' => $id]))->with('status','Este cadastro não está ativo, por esse motivo não será possível cadastrar dependentes! Você deve primeiro ativar o cadastro para depois cadastrar dependentes novamente.');
        }

        //cria e salva um dependente de acordo com os dados que são enviados e validados pelo sistema
        $dependente = new Dependente();
        $dependente->nome = $request->input('cNomeDep');
        $dependente->data_nascimento = $request->input('cDataNasc');
        $dependente->pessoa_id = $pessoa->id;
        $dependente->save();

        //direciona para a view dos dependetes da pessoa
        return redirect(route('dependente',['id' => $pessoa->id]));
    }

    //apaga um dependente
    public function deleteDependente($id,$pessoa)
    {
        //procurar a pessoa a quem o dependente esta vinculado
        $pessoa = Pessoa::find($pessoa);
        //procura o dependete
        $dependente = Dependente::find($id);


        if (isset($dependente)){
            //apaga o dependete e volta para a tela de listagem de dependentes da pessoa
            $dependente->delete();
            return redirect(route('dependente',['id' => $pessoa->id]));
        }
    }

    //muda o status quando o btn é clicado
    //se estiver ativo muda para inativo e vice-versa
    public function status($id){
        $pessoa = Pessoa::find($id);
        if ($pessoa->status == true){
            $pessoa->status = false;
            $pessoa->save();
        }else
            $pessoa->status = true;
            $pessoa->save();
        return redirect('lista');
    }

    //direciona para a view que vai editar a pessoa passando seus dados
    public function edit($id)
    {
        $pessoa = Pessoa::find($id);
        return view('Cadastro.editar', compact('pessoa'));
    }

   //faz o update da pessoa
    public function update(Request $request,$id)
    {
        //regra de validação
        $regras = [
            'cFoto' => 'max:200'
        ];
        //mensagem personalizada
        $mensagen = [
            'cFoto.max' => 'A foto não pode ter o tamanho maior que 200kb',
            'cDataNasc' => 'A idade não pode ser maior que 120 anos'
        ];
        //valida os campos de acordo com as regras
        $request->validate($regras,$mensagen);

        /*procura a pessoa para que seja feito o update das suas informações*/
        $pessoa = Pessoa::find($id);

        //verifica se esta sendo enviado uma nova foto
        if (!$request->file('cFoto') == '') // verifica se a foto foi alterada
        {
            $foto = $request->file('cFoto')->store('images','public');
        }
        else{
            //caso não venha nenhuma foto usa a que ja tem no sistema
            $foto = $pessoa->foto;
        }
        //faz o update da pessoa encontrada
        $pessoa->nome = $request->input('cNome');
        $pessoa->data_nascimento = $request->input('cDataNasc');
        $pessoa->email = $request->input('cEmail');
        $pessoa->foto = $foto;
        $pessoa->save();

        return redirect(route('lista'));
    }

    //apaga uma ou varias pessoas e tambem seus dependentes
    public function apagaCheckBox(Request $request)
    {
        //recebe os dados passados via request pelo form
        $pessoas = $request->all();

        /* este if verifica se tem alguma pessoa para excluir, caso não tenha direciona para a lista de pessoas*/
        if (count($pessoas)>0){
            /*se ele encontrar alguma pessoa primeiro procura os dependentes dela e exclui
            depois exclui a pessoa e direciona para a listagem de pessoas*/
            foreach ($pessoas['checkbox'] as $item => $pessoa) {
                $dep = Dependente::where([ 'pessoa_id' => $pessoa])->delete();
                $p = Pessoa::find($pessoa);
                $p->delete();
            }
            return redirect('lista');
        }else
        return redirect('lista');
    }
}
