<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html"; charset="UTF-8" />
    <meta content="{{csrf_token()}}" name="csrf-token"/>

    <link type="text/css" rel="stylesheet" href="{{asset('css/principal.css')}}" />




    <title>Cadastro de Pessoas</title>
</head>

<body>

<div id="conteudoGeral">

    <div id="topoGeral">
        <div id="logoTopo" onclick="location.href='{{route('index')}}'" style="cursor:pointer;"></div>
        <div id="dirTopo"></div>
    </div>

    <div id="baixoGeral">

        <div id="menuEsq">
            <div id="titMenu">Menu de Opções <br></div>
            <a href="{{route('index')}}">Início</a>
            <a href="{{route('lista')}}">Listar Cadastros</a>
            <a href="{{route('cadastro')}}">Incluir Novo</a>
        </div>

        <div id="conteudoDir">
