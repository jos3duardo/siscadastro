@extends('Layout.index')
@section('content')

{{--    {{dd($pessoa)}}--}}
    <div id="listaPessoas">
        <h1>Dependentes</h1>

        <div id="infoDep">

            <div id="fotoCadastro">
                <img src="/storage/{{$pessoa->foto}}" width="77" height="77" />
            </div>

            <table id="tListaCad" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td class="tituloTab">Nome</td>
                    <td>{{$pessoa->nome}}</td>
                </tr>
                <tr bgcolor="#cddeeb">
                    <td class="tituloTab">Data de Nascimento</td>
                    <td>{{Carbon\Carbon::parse($pessoa->data_nascimento)->format('d/m/Y')}}</td>
                </tr>
                <tr>
                    <td class="tituloTab">Email</td>
                    <td>{{$pessoa->email}}</td>
                </tr>
            </table>
            {{-- inicio do cadastro de um dependente--}}
            <form id="frmAdicionaDep" action="{{route('addDependente', ['id' => $pessoa->id])}}" method="post">
                @csrf
                <div class="agrupa mB mR">
                    <label for="cNomeDep">Nome</label><br />
                    <input autofocus type="text" name="cNomeDep" id="cNomeDep" required />
                </div>
                <div class="agrupa">
                    <label for="cDataNasc">Data de Nascimento</label><br />
                    <input type="date"  name="cDataNasc" id="cDataNasc"  required/>
                </div>
                <button class="btPadrao">Adicionar</button>
            </form>
            {{-- fim do cadastro do dependete--}}

            {{-- Inicio da listagem dos dependentes cadastrados para a pessoa --}}
            @if(count($dependentes) <= 0)
                <br><br>
                <h5>Nenhum dependente cadastrado para <b>{{$pessoa->nome}}</b>!</h5>
                <br><br>
            @else
            <table id="tLista" cellpadding="0" cellspacing="0" border="0">
                <thead>
                    <tr>
                        <th width="60%" class="tL">Nome do Dependente</th>
                        <th width="33%">Data de Nascimento</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($dependentes as $dependente)
                        <tr>
                            <td>{{$dependente->nome}}</td>
                            <td align="center">{{Carbon\Carbon::parse($dependente->data_nascimento)->format('d/m/Y')}}</td>
                            <td align="center"><a href="{{route('deleteDependente',['id' => $dependente->id, 'pessoa' => $pessoa->id])}}" class="btRemover"
                                  onclick="return confirm('Tem certeza que quer excluir - {{$dependente->nome}} - dependente de {{$pessoa->nome}} definitivamente ?')"
                                ></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
            <a href="{{route('lista')}}" class="btPadrao mT">Salvar</a>
        </div>
    </div><br><br>
    {{-- mensagem para erro caso a foto ultrapasse o tamanho maximo--}}
    @if (session('status'))
        <div class="alert alert-success">
            <li style="color: red">{{ session('status') }}</li>
        </div>
    @endif
@endsection
