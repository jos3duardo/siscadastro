@extends('Layout.index')
@section('content')
    <div id="listaPessoas">

    <h1>Incluindo um Novo Cadastro</h1>

    <form id="formCadastrar" method="post" enctype="multipart/form-data" action="{{route('addCadastro')}}">
        @csrf
        <label for="cNome">Nome</label><br />
        <input id="cNome" name="cNome" autofocus required/><br />
        <label for="cDataNasc">Data de Nascimento</label><br />
        <input id="cDataNasc" name="cDataNasc" type="date" required /><br />
        <label for="cEmail">E-Mail</label><br />
        <input id="cEmail" name="cEmail"  type="email" required/><br />
        <label for="cFoto">Foto</label><br />
        <input id="cFoto" name="cFoto" type="file" accept="image/*"/><br />
        <button type="submit" class="btPadrao">Salvar</button>
    </form>
    <br><br>
    {{-- mensagem para erro caso a foto ultrapasse o tamanho maximo--}}
    @if ($errors->any())
        <div>
            <ul>
                @foreach ($errors->all() as $error)
                    <li style="color: red">{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (session('status'))
        <div class="alert alert-success">
            <li style="color: red">{{ session('status') }}</li>
        </div>
    @endif
</div>


@endsection
