@extends('Layout.index')
@section('content')

<div id="listaPessoas">

    <h1>Editando o Cadastro</h1>

    <form id="formCadastrar" method="post" enctype="multipart/form-data" action="{{route('updateCadastro',['id' => $pessoa->id])}}">
        @csrf
        <label for="cNome">Nome</label><br />
        <input id="cNome" name="cNome" value="{{$pessoa->nome}}"  required/><br />
        <label for="cDataNasc">Data de Nascimento</label><br />
        <input id="cDataNasc" name="cDataNasc" type="date" value="{{$pessoa->data_nascimento}}" required/><br />
        <label for="cEmail">E-Mail</label><br />
        <input id="cEmail" name="cEmail" value="{{$pessoa->email}}" required/><br />
        <label for="cFoto">Foto</label><br />
        <input id="cFoto" name="cFoto" type="file"/><br />
        <button class="btPadrao">Salvar</button>
    </form>
    <br><br>
    {{-- mensagem para erro caso a foto ultrapasse o tamanho maximo--}}
    @if ($errors->any())
        <div>
            <ul>
                @foreach ($errors->all() as $error)
                    <li style="color: red">{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

{{--    <a href="javascript:;" class="btPadrao">Salvar</a>--}}

</div>
@endsection
