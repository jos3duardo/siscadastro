@extends('Layout.index')
@section('content')
    <div id="listaPessoas">
        <h1>Cadastros</h1>
        @if(count($pessoa) <= 0)
            <h3>Ainda não há pessoas cadastradas</h3>
            <a href="{{route('cadastro')}}" class="btPadraoExcluir">Cadastrar</a>
        @else
    <form action="{{route('apagaCheckBox')}}" method="get" id="formulario">
    <a href="javascript:{}" class="btPadraoExcluir" onclick = "Aviso(); Enviar()">Excluir</a>
        <table id="tLista" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <th width="5%"><input type="checkbox" class="checkAll" id="checkAll" /></th>
                <th width="10%">ID</th>
                <th class="tL">Nome</th>
                <th width="20%">Data de Nascimento</th>
                <th width="10%">Dep</th>
                <th width="8%">Status</th>
            </tr>
            @foreach($pessoa as $p)
            <tr>
                <td align="center" style="border-left:0;"><input class="chM" type="checkbox" name="checkbox[{{$p->id}}]" value="{{$p->id}}"/></td>
                <td align="center">{{$p->id}}</td>
                <td><a href="{{route('editarCadastro',['id' => $p->id])}}" class="linkUser">{{$p->nome}}</a></td>
                <td align="center">{{Carbon\Carbon::parse($p->data_nascimento)->format('d/m/Y')}}</td>
                <td align="center"><a href="{{route('dependente',['id' => $p->id])}}" class="btAdicionar"></a></td>
                <td align="center"><a href="{{route('status',['id' => $p->id])}}" @if($p->status) ? class="btVerde" : class="btCinza" @endif class="btCinza"></a></td>
            </tr>
            @endforeach
        </table>
    </form>
    </div>
    <div id="paginacao">
        <div class="content">
            <a href="{{$pessoa->previousPageUrl()}}" class="btSeta1"></a>
            <div id="pags">{{$pessoa->firstItem()}} de {{$pessoa->lastItem()}}</div>
            <a href="{{$pessoa->nextPageUrl()}}" class="btSeta2"></a>
            <select id="paginas">
                @for($inicio = 1; $inicio <= $totalPaginas; $inicio++)
                    <a href="{{$pessoa->url($inicio)}}"><option>{{$inicio}}</option></a>
                @endfor
            </select>
        </div>
    </div>
    @endif
@endsection
